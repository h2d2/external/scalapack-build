#!/bin/bash

function usage
{
   echo
   echo "Usage:"
   echo "   gen_one.sh (openmpi, mpich2, deinompi) (dynamic, static)"
   exit 126
}

INST_DIR=$INRS_LXT/scalapack-2.1.0
TGT_LIB=$1; shift
case ${TGT_LIB%%-*} in
   mpich2)
      MPI_DIR=$INRS_LXT/mpich2-1.2.1p1/_install
      MPI_BIN=$MPI_DIR/bin
      MPI_LIB=$MPI_DIR/lib
      ;;
   openmpi)
      MPI_DIR=$INRS_LXT/$TGT_LIB
      MPI_BIN=$MPI_DIR/bin
      MPI_LIB=$MPI_DIR/lib
      ;;
   deinompi)
      MPI_DIR=$INRS_LXT/DeinoMPI-2.0.1
      MPI_BIN=$MPI_DIR/bin
      MPI_LIB=$MPI_DIR/lib
      ;;
   *)
      echo "Error: Invalid mpi."
      echo "    Valid values are (openmpi, mpich2, deinompi)"
      echo "    Got: ${TGT_LIB%-*} from $TGT_LIB"
      usage
      ;;
esac

INST_BUILD=$1; shift
case $INST_BUILD in
   dynamic)
      INST_FLAGS='-O3 -fPIC'
      ;;
   static)
      INST_FLAGS='-O3'
      ;;
   *)
      echo "Error: Invalid build. Valid values are (dynamic, static)"
      usage
      ;;
esac

# ---  Set the PATH
# Make sure to keep actual Python in front, to prevent
# the case where MPI_BIN points to /usr/bin and would
# change python
PYTHON_DIR=$(dirname `which python`)
export PATH=$PYTHON_DIR:$MPI_BIN:$PATH
export LD_LIBRARY_PATH=$MPI_LIB:$LD_LIBRARY_PATH

# ---  Clean
rm -rf $INST_DIR/build
rm -rf $INST_DIR/log
python ./setup.py --clean

# ---  Compile
python ./setup.py --notesting --prefix="$INST_DIR" --mpibindir="$MPI_BIN" --ccflags="$INST_FLAGS" --fcflags="$INST_FLAGS"

# ---  Move result
if !([ -d "$INST_DIR/lib/$TGT_LIB/$INST_BUILD" ])
then
   mkdir -p $INST_DIR/lib/$TGT_LIB/$INST_BUILD
fi
mv -f $INST_DIR/lib/*.a $INST_DIR/lib/$TGT_LIB/$INST_BUILD
