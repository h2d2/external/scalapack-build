@echo off
setlocal EnableExtensions

call :main %*
goto :EOF

:usage
   echo
   echo "Usage:"
   echo "   gen_one.sh (openmpi, mpich2, deinompi) (dynamic, static)"
   exit
goto :EOF

:get_target_lib
    set TGT_LIB=%1
    if [%TGT_LIB%]==[mpich2]   goto get_target_lib_mpich2
    if [%TGT_LIB%]==[openmpi]  goto get_target_lib_openmpi
    if [%TGT_LIB%]==[deinompi] goto get_target_lib_deinompi
    echo "Error: Invalid mpi library. Valid values are (openmpi, mpich2, deinompi)"
    goto error

    :get_target_lib_mpich2
        set MPI_DIR=%INRS_LXT%/mpich2-1.2.1p1/_install
        set MPI_BIN=%MPI_DIR%/bin
        set MPI_LIB=%MPI_DIR%/lib
        set MPI_INC=%MPI_DIR%/include
        goto get_target_lib_done
    :get_target_lib_openmpi
        set MPI_DIR=%INRS_LXT%/openmpi-1.4.3
        set MPI_BIN=%MPI_DIR%/bin
        set MPI_LIB=%MPI_DIR%/lib
        set MPI_INC=%MPI_DIR%/include
        goto get_target_lib_done
    :get_target_lib_deinompi
        set MPI_DIR=%INRS_LXT%/DeinoMPI-2.0.1
        set MPI_BIN=%MPI_DIR%/bin
        set MPI_LIB=%MPI_DIR%/lib
        set MPI_INC=%MPI_DIR%/include
        goto get_target_lib_done

    :get_target_lib_done
goto :EOF

:get_cplr_flags
    set INST_BUILD=%1
    if [%INST_BUILD%]==[dynamic] goto get_cplr_flags_dynamic
    if [%INST_BUILD%]==[static]  goto get_cplr_flags_static
    echo "Error: Invalid build. Valid values are (dynamic, static)"
    goto error

    :get_cplr_flags_dynamic
        set INST_FLAGS=-O3 -fPIC
        goto get_cplr_flags_done
    :get_cplr_flags_static
        set INST_FLAGS=-O3
        goto get_cplr_flags_done

    :get_cplr_flags_done
goto :EOF

:main
    set PWD_DIR=%~dp0
    set PWD_DIR=%PWD_DIR:~0,-1%

    call :get_target_lib %1 & shift
    call :get_cplr_flags %1 & shift
    if %errorLevel% NEQ 0 (exit /b )

    set INST_DIR=%INRS_LXT%/scalapack-2.0.2
    set PATH=%MPI_BIN%;%PWD_DIR%;%PATH%

    :: ---  Clean
    rmdir /Q /S -rf %INST_DIR%/build
    rmdir /Q /S  %INST_DIR%/log
    python ./setup.py --clean

    :: ---  Compile
    set ARGS=
    set ARGS=%ARGS% --prefix=%INST_DIR%
    set ARGS=%ARGS% --ccflags="%INST_FLAGS%"
    set ARGS=%ARGS% --fcflags="%INST_FLAGS%"
    python ./setup.py --notesting %ARGS%

    :: ---  Move result
    set TGT_DIR=%INST_DIR%/lib/%TGT_LIB%/%INST_BUILD%
    if not exist "%TGT_DIR%" mkdir "%TGT_DIR%"
    mv -f %INST_DIR%/lib/*.a "%TGT_DIR%"
goto :EOF

:error
    exit /b 2
